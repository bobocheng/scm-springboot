package test.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.dao.ITestDao;
import test.service.ITestService;
import cn.zhys513.common.cache.annotation.ClearCacheThroughNSpace;
import cn.zhys513.common.cache.annotation.ReadThroughCache;

/**
 * 
 * @author zhys
 * @date 2014-5-8 上午11:22:24
 */
@Service
public class TestService implements ITestService {

    @Autowired
    ITestDao testDao;
    
    @ReadThroughCache()
    @Override
    public String test() {
        return testDao.test();
    }
    
    @ReadThroughCache(nameSpace = "sysn",key = "${0}${1}")
    @Override
    public String test(String str1, String str2) {
        return testDao.test();
    }
 
	@Override
    @ReadThroughCache(nameSpace = "sysn")
	public String version(String version) { 
		java.util.Random r = new java.util.Random(); 
        String s = String.valueOf(r.nextInt());
		return s;
	}
	
	@Override
	@ClearCacheThroughNSpace(nameSpace = "sysn")
	public String clearCache(String version) { 
		java.util.Random r = new java.util.Random(); 
        String s = String.valueOf(r.nextInt());
		return s;
	}

}
