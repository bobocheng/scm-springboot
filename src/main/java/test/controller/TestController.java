package test.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import test.service.ITestService;
import cn.zhys513.common.cache.CacheProvider;

import com.alibaba.fastjson.JSON;

@Controller
public class TestController {

    @Resource(name = "cachedProvider")
    CacheProvider cache;
    
    @Autowired
    ITestService testService;
    
	@RequestMapping(value={"/cache/","/cache"}) 
//	@ResponseBody
	public String cached(Map<String,Object> map ) { 
//	    String s = testService.test(); 
	    String s = testService.test("2222","3333"); 
	    map.put("hello", s);
		System.out.println("hello:" + s); 
		return "hello";
	} 
	  
	@RequestMapping(value={"/clear/","/clear"})
	@ResponseBody
	public String clear() {  
		testService.clearCache("1");
		return "清楚缓存成功.";
	}
	
}
